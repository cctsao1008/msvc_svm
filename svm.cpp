// svm.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//#include "pch.h"

#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES

#include <iostream>
#include <windows.h>

#include <stdio.h>
#include <math.h>
#include <time.h>

void svm(uint32_t count, FILE *fp);

int main()
{
    //std::cout << "Hello World!\n";

    FILE* fp;

    time_t now;

    /* Time Function for filename definition */
    now = time(NULL);
    struct tm* t = localtime(&now);
    char name[30];

    strftime(name, sizeof(name), "svm_%d%m%Y%H%M%S.csv", t);

    fp = fopen(name, "w+");

    for (uint16_t i = 0; i <= 360; i++)
        svm(i, fp);

    fclose(fp);
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started:
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file

typedef enum _fet_sector {
    SECTOR_1 = 0x0,
    SECTOR_2 = 0x1,
    SECTOR_3 = 0x2,
    SECTOR_4 = 0x3,
    SECTOR_5 = 0x4,
    SECTOR_6 = 0x5
} FET_SECTOR;

void svm(uint32_t count, FILE *fp)
{
    float theta = (float)count;
    float ta = 0.0f, tb = 0.0f, t0 = 0.0f;
    float va = 0, vb = 0, vc = 0, Ts = 1.0f;// , M = 200;
    uint16_t sector = 0;
    //static uint16_t sector_old = 0, sector_changed = 0;
    uint32_t pwm[3] = { 0 };

    theta = (float)((uint16_t)theta % 360); // theta between 0 ~ 360 degree
    sector = (uint16_t)(theta / 60); // 60 degree per sector

    theta = theta - sector * 60.0f; // theta between va & vb

    printf("theta = %f, sector = %d\n", theta, sector);

    theta = theta * ((float)M_PI / 180.0f); // degree to radius

    ta = Ts * sinf(((float)M_PI / 3.0f) - theta);
    tb = Ts * sinf(theta);
    t0 = Ts - ta - tb;

    switch (sector)
    {
    case SECTOR_1:

        va = (ta + tb + 0.5f * t0); // phase 120 degree
        vb = (tb + 0.5f * t0); // phase 0 degree
        vc = (0.5f * t0); // phase 240 degree

        break;

    case SECTOR_2:

        va = (ta + 0.5f * t0); // phase 180 degree
        vb = (ta + tb + 0.5f * t0); // phase 60 degree
        vc = (0.5f * t0);

        break;

    case SECTOR_3:

        va = (0.5f * t0); // phase 240 degree
        vb = (ta + tb + 0.5f * t0); // phase 120 degree
        vc = (tb + 0.5f * t0);

        break;

    case SECTOR_4:

        va = (0.5f * t0); // phase 300 degree
        vb = (ta + 0.5f * t0);
        vc = (ta + tb + 0.5f * t0);

        break;

    case SECTOR_5:

        va = (tb + 0.5f * t0); // phase 360/0 degree
        vb = (0.5f * t0);
        vc = (ta + tb + 0.5f * t0);

        break;

    case SECTOR_6:

        va = (ta + tb + 0.5f * t0); // phase 60 degree
        vb = (0.5f * t0);
        vc = (ta + 0.5f * t0);

        break;

    default:
        break;
    }

    va = va - 0.5f;
    vb = vb - 0.5f;
    vc = vc - 0.5f;

    printf("va = %7.4f, vb = %7.4f, vc = %7.4f\n", va, vb, vc);

    fprintf(fp, "%7.4f, %7.4f, %7.4f\n", va, vb, vc);

#if 0
    /* SVPWM */
    pwm[2] = SV_PWM_DUTY_MAX * va;
    pwm[1] = SV_PWM_DUTY_MAX * vb;
    pwm[0] = SV_PWM_DUTY_MAX * vc;

    if (pwm[2] <= SV_PWM_DUTY_MIN)
        pwm[2] = SV_PWM_DUTY_MIN;

    if (pwm[1] <= SV_PWM_DUTY_MIN)
        pwm[1] = SV_PWM_DUTY_MIN;

    if (pwm[0] <= SV_PWM_DUTY_MIN)
        pwm[0] = SV_PWM_DUTY_MIN;

    timer_channel_output_pulse_value_config(TIMER0, TIMER_CH_2, pwm[2]);
    timer_channel_output_pulse_value_config(TIMER0, TIMER_CH_1, pwm[1]);
    timer_channel_output_pulse_value_config(TIMER0, TIMER_CH_0, pwm[0]);

    /* SVM output voltage waveform */
    dac_concurrent_data_set(DAC_ALIGN_12B_R, (uint16_t)(va * 4095), (uint16_t)(vb * 4095));
#endif
}

